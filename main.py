from fastapi import FastAPI
from fastapi.responses import HTMLResponse
from config.database import engine, Base
from middlewares.error_handler import ErrorHandler
from routers.computer import computer_router
from routers.user import user_router

app = FastAPI()
app.title = "Mi primera chamba con FastAPI"
app.version = "0.0.1"

app.add_middleware(ErrorHandler)
app.include_router(computer_router)
app.include_router(user_router)

Base.metadata.create_all(bind=engine)

website = """
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pruebas de APIs de FastAPI</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 0;
            background-color: #f4f4f4;
        }
        .container {
            width: 80%;
            margin: auto;
            overflow: hidden;
        }
        header {
            background: #35424a;
            color: #ffffff;
            padding-top: 30px;
            min-height: 70px;
            border-bottom: #e8491d 3px solid;
        }
        header a {
            color: #ffffff;
            text-decoration: none;
            text-transform: uppercase;
            font-size: 16px;
        }
        header ul {
            padding: 0;
            list-style: none;
        }
        header li {
            display: inline;
            padding: 0 20px 0 20px;
        }
        .main-content {
            padding: 20px;
            background: #ffffff;
            margin-top: 10px;
        }
        footer {
            background: #35424a;
            color: #ffffff;
            text-align: center;
            padding: 10px 0;
            margin-top: 20px;
        }
        .api-test {
            margin-bottom: 20px;
        }
        label {
            display: block;
            margin-bottom: 5px;
        }
        input, button {
            padding: 10px;
            margin-bottom: 10px;
            width: 100%;
            box-sizing: border-box;
        }
        button {
            background: #e8491d;
            color: #ffffff;
            border: none;
            cursor: pointer;
        }
        button:hover {
            background: #333;
        }
        pre {
            background: #f4f4f4;
            padding: 10px;
            border: 1px solid #ddd;
        }
    </style>
</head>
<body>
    <header>
        <div class="container">
            <h1><a href="#">Pruebas de APIs de FastAPI</a></h1>
        </div>
    </header>
    <div class="container main-content">
        <h2>Prueba tus APIs</h2>
        <div class="api-test">
            <label for="endpoint">Endpoint de la API:</label>
            <input type="text" id="endpoint" placeholder="Ejemplo: /items/">
            <button onclick="testApi()">Probar API</button>
        </div>
        <div id="response">
            <h3>Respuesta:</h3>
            <pre id="apiResponse"></pre>
        </div>
    </div>
    <footer>
        <p>FastAPI &copy; 2024</p>
    </footer>
    <script>
        async function testApi() {
            const endpoint = document.getElementById('endpoint').value;
            const url = `http://159.89.54.163${endpoint}`;
            try {
                const response = await fetch(url);
                const data = await response.json();
                document.getElementById('apiResponse').innerText = JSON.stringify(data, null, 2);
            } catch (error) {
                document.getElementById('apiResponse').innerText = 'Error al conectar con la API';
            }
        }
    </script>
</body>
</html>
"""

@app.get('/', tags=['home'])
def message():
    return HTMLResponse(website)
    # return HTMLResponse('<h1> Hello World This is a change! this is a second change </h1>')

