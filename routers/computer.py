from fastapi import Path, Query
from fastapi.responses import JSONResponse
from typing import List
from config.database import Session
from models.computer import Computer as ComputerModel
from fastapi.encoders import jsonable_encoder
from fastapi import APIRouter
from services.computer import ComputerService
from schemas.computer import Computer

computer_router = APIRouter()


@computer_router.get(
        '/computers',
        tags=['computers'],
        response_model=List[Computer],
        status_code=200
        )
def get_computers() -> List[Computer]:
    db = Session()
    result = ComputerService(db).get_computers()
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


@computer_router.get(
        '/computers/{id}',
        tags=['computers'],
        response_model=Computer
        )
def get_computer(id: int = Path(ge=1, le=2000)) -> Computer:
    db = Session()
    result = ComputerService(db).get_computer(id)
    if not result:
        return JSONResponse(status_code=404, content={'message': "No encontrado"})
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


@computer_router.get(
        '/computers/',
        tags=['computers'],
        response_model=List[Computer]
        )
def get_computers_by_brand(brand: str = Query(max_length=15)):
    db = Session()
    result = ComputerService(db).get_computers_by_brand(brand)
    return JSONResponse(status_code=200, content=jsonable_encoder(result))


@computer_router.post(
        '/computers',
        tags=['computers'],
        response_model=dict,
        status_code=200
        )
def create_computer(computer: Computer) -> dict:
    db = Session()
    result = db.query(ComputerModel).filter(
            ComputerModel.id == computer.id).first()
    if result:
        return JSONResponse(status_code=403, content={'message': "Ese ID ya esta ocupado"})
    ComputerService(db).create_computer(computer)
    return JSONResponse(status_code=200, content={"message": "Se ha registrado la computadora"})


@computer_router.put(
        '/computers/{id}',
        tags=['computers'],
        response_model=dict,
        status_code=200
        )
def update_computer(id: int, computer: Computer) -> dict:
    db = Session()
    result = ComputerService(db).get_computer(id)
    if not result:
        return JSONResponse(status_code=404, content={'message': "No encontrado"})

    ComputerService(db).update_computer(id, computer)
    return JSONResponse(status_code=202, content={'message': "Se ha modificado la computadora"})


@computer_router.delete(
        '/computers/{id}',
        tags=['computers'],
        response_model=dict,
        status_code=200
        )
def delete_computer(id: int) -> dict:
    db = Session()
    result = db.query(ComputerModel).filter(ComputerModel.id == id).first()
    if not result:
        return JSONResponse(status_code=404, content={'message': "No encontrado"})
    ComputerService(db).delete_computer(id)
    return JSONResponse(status_code=200, content={"message": "Se ha Eliminado la computadora"})
