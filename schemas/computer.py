from pydantic import BaseModel, Field
from typing import Optional


class Computer(BaseModel):
    id: Optional[int] = None
    model: str = Field(min_length=5, max_length=15)
    brand: str = Field(max_length=50)
    color: str = Field(max_length=15)
    ram: str = Field(max_length=10)
    storage: str = Field(max_length=10)

    class Config:
        json_schema_extra = {
                "example": {
                    "id": 1,
                    "model": "Pulse GL-16",
                    "brand": "MSI",
                    "color": "Black",
                    "ram": "8GB",
                    "storage": "100GB"
                    }
                }
